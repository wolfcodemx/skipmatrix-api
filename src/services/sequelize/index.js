import Promise from 'bluebird'
import { mysql } from '../../config'
const Sequelize = require("sequelize");

const sequelize = new Sequelize(mysql.DB, mysql.USER, mysql.PASSWORD, {
  host: mysql.HOST,
  dialect: mysql.dialect,
  operatorsAliases: false,

  pool: {
    max: mysql.pool.max,
    min: mysql.pool.min,
    acquire: mysql.pool.acquire,
    idle: mysql.pool.idle
  }
});

export default sequelize
