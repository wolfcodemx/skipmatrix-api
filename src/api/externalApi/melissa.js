import _ from 'underscore'
import { doCurls, curl } from '../helpers/curls'
import { sliceIntoChunks } from '../helpers/utils'
const encode = require('nodejs-base64-encode');

const MAX_PAYLOADS_PER_REQUEST = 100

const LIVE = true
const URL = 'https://property.melissadata.net/v4/WEB/LookupProperty'
const URL_PERSONATOR = ''
const API_KEY_PRODUCTION = 'fsErIPv0_chd9aGLw1d5lF**'
const API_KEY_SANDBOX = 'GzgR5zW4Mv3rxEuIlQtyDw**'

export const search = async (leads, tracerType, searchType) => {
    let apiKey = getApikey()  
    let payloads = leadsToPayload(leads, searchType)
    let chunks = sliceIntoChunks(payloads, MAX_PAYLOADS_PER_REQUEST)
    let responsesProcessed = []
    let responses = []
    let tracerWithVendorHits = 0
    for (let i = 0 ; i < chunks.length; i++) {

        let payload = {
            CustomerId: apiKey,
            Columns: 'GrpAll',
            Format: 'json',
            TotalRecords: chunks[i].length,
            Records: chunks[i]
        }

        responses.push(await doCurls([{
            url: getUrl(),
            headers: getHeaders(),
            payload
        }]))
    }
    responses.forEach((response) => {
        let responseArray = response[0].response.Records
        responseArray.forEach((responseChild) => {
            let index = leads.findIndex((element) => element.tempLeadID.toString() == responseChild.RecordID)
            let newResponse = {}
            //newResponse['melissa'] = response
            newResponse['tempLeadID'] = leads[index]['tempLeadID'] || null
            newResponse['batchID'] = leads[index]['batchID'] || null
            newResponse['vacant'] = leads[index]['vacant'] || null
            newResponse['vacantMailing'] = leads[index]['vacantMailing'] || null
            newResponse['duplicateID'] = leads[index]['duplicateID'] || null
            newResponse['validatedMailingStreet'] = leads[index]['validatedMailingStreet'] || null
            newResponse['validatedMailingCity'] = leads[index]['validatedMailingCity'] || null
            newResponse['validatedMailingZipCode'] = leads[index]['validatedMailingZipCode'] || null
            newResponse['validatedMailingState'] = leads[index]['validatedMailingState'] || null
            newResponse['validatedMailingCounty'] = leads[index]['validatedMailingCounty'] || null
            tracerWithVendorHits += 1
            responsesProcessed.push({...newResponse, ...responseChild})
        })
    })
    return {vendorTraces: responsesProcessed, tracerWithVendorHits}
}

const getUrl = () => {
    return URL
}

const getHeaders = () => {
    return [ 
        'Content-Type: application/json',
        'Accept: application/json'
    ]
}

const getApikey = () => {
    return LIVE ? API_KEY_PRODUCTION : API_KEY_SANDBOX
}

const getLoginCreds = () => {
    return LIVE ? {
        url: PRODUCTION_LOGIN_ULR,
        username: PRODUCTION_USERNAME,
        password: PRODUCTION_PASSWORD
    } : {
        url: SANDBOX_LOGIN_URL,
        username: SANDBOX_USERNAME,
        password: SANDBOX_PASSWORD
    }
}

const leadsToPayload = (leads, searchType) => {
    return leads.map((lead) => {
        let tempID = lead.tempLeadID
        let firstName = (lead.validatedFirstName || lead.firstName || null)
        let lastName = (lead.validatedLastName || lead.lastName || null)
        let apn = (lead.parcelNumber || null)
        let fips = lead.FIPSCode || null
        let address = lead.validatedPropertyStreet || lead.propertyStreet
        let city = lead.validatedPropertyCity || lead.propertyCity
        let state = lead.validatedPropertyState || lead.propertyState
        let zip = lead.validatedPropertyZipCode || lead.propertyZipCode
    
        if (searchType == 'apn') 
            return {RecordID: tempID.toString(), APN: apn, FIPS: fips}
        else if (searchType == 'streetCityState') 
            return {RecordID: tempID.toString(), AddressLine1: address, City: city, State: state}
        else if (searchType == 'streetZip') 
            return {RecordID: tempID.toString(), AddressLine1: address, PostalCode: zip}
        else 
            return {RecordID: tempID.toString(), AddressLine1: address, City: city, State: state, PostalCode: zip, APN: apn, FIPS: fips}
    })
    
}

