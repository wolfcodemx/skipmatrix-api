import _ from 'underscore'
import { doCurls } from '../helpers/curls'
import { sliceIntoChunks } from '../helpers/utils'

const AUTH_ID = 'a1f27f5a-9599-6036-1c5b-95b3618b1fe2'
const AUTH_TOKEN = 'fPiveKa3c0TuZ2nvLD0z'
const MAX_PAYLOADS_PER_REQUEST = 50

export const search = async (leads) => {
    let payloads = leadsToPayload(leads)
    let chunks = sliceIntoChunks(payloads, MAX_PAYLOADS_PER_REQUEST)
    let responses = []
    for (let i = 0 ; i < chunks.length; i++) {
        responses.push(await doCurls([{
            url: getUrl(),
            headers: getHeaders(),
            payload: chunks[i]
        }]))
    }
    responses.forEach((response) => {
        let smartyResult = response[0].response[0]
        let inputIdSplitted = smartyResult.input_id.split(':')
        let leadIndex = leads.findIndex((element) => inputIdSplitted[0] == element.tempLeadID.toString())

        let vacant = smartyResult.analysis.dpv_vacant == 'Y' ? 'Yes' : smartyResult.analysis.dpv_vacant == 'N' ? 'No' : 'Unknown'
        if (inputIdSplitted[1] == 'M') {
            leads[leadIndex]['validatedMailingStreet'] = smartyResult.delivery_line_1
            leads[leadIndex]['validatedMailingCity'] = smartyResult.components.default_city_name
            leads[leadIndex]['validatedMailingZipCode'] = smartyResult.components.zipcode
            leads[leadIndex]['validatedMailingState'] = smartyResult.components.state_abbreviation
            leads[leadIndex]['validatedMailingCounty'] = smartyResult.analysis.dpv_vacant
            leads[leadIndex]['validatedMailingCountyFIPS'] = smartyResult.metadata.county_fips
            leads[leadIndex]['vacantMailing'] = vacant
        } else if (inputIdSplitted[1] == 'P') {
            leads[leadIndex]['validatedPropertyStreet'] = smartyResult.delivery_line_1
            leads[leadIndex]['validatedPropertyCity'] = smartyResult.components.default_city_name
            leads[leadIndex]['validatedPropertyZipCode'] = smartyResult.components.zipcode
            leads[leadIndex]['validatedPropertyState'] = smartyResult.components.state_abbreviation
            leads[leadIndex]['validatedPropertyCounty'] = smartyResult.analysis.dpv_vacant
            leads[leadIndex]['validatedPropertyCountyFIPS'] = smartyResult.metadata.county_fips
            leads[leadIndex]['vacant'] = vacant
        }
    })
    return leads
}

const getUrl = () => {
    return `https://us-street.api.smartystreets.com/street-address?auth-id=${AUTH_ID}&auth-token=${AUTH_TOKEN}`
}

const getHeaders = () => {
    return [ 
        'Content-Type: application/json',
        'charset: utf-8'
    ]
}

const leadsToPayload = (leads) => {
    return leads.map((lead) => {
        if (!_.isEmpty(lead['mailingStreet'])) {
            return {street: lead['mailingStreet'], city: lead['mailingCity'], state: lead['mailingState'], zip: lead['mailingZipCode'], candidates: 1, input_id: lead['tempLeadID']+':M'}
        } else if (!_.isEmpty(lead['propertyStreet'])) {
            return {street: lead['propertyStreet'], city: lead['propertyCity'], state: lead['propertyState'], zip: lead['propertyZipCode'], candidates: 1, input_id: lead['tempLeadID']+':P'}
        } else {
            return {}
        }
    })
    
}

