import _ from 'underscore'
import { doCurls, curl } from '../helpers/curls'
import { sliceIntoChunks } from '../helpers/utils'
const encode = require('nodejs-base64-encode');

const MAX_PAYLOADS_PER_REQUEST = 50

var TOKEN = ''

export const search = async (leads, tracerType) => {
    console.log({IDI_Mode: String(process.env.IDI_LOGIN_URL).includes('test') ? 'Test' : 'Production'})
    let token = await getToken()
    TOKEN = token    
    let payloads = leadsToPayload(leads, tracerType)
    let responses = []
    let responsesProcessed = []
    let tracerWithVendorHits = 0
    for(let i = 0; i < payloads.length; i++) {
        responses.push(await doCurls([{
            url: getUrl(),
            headers: getHeaders(),
            payload: payloads[0]
        }]))
    }
    responses.forEach((response, index) => {
        let newResponse = {}
        if (_.isUndefined(response[0]))
            return

        newResponse['idi'] = response[0].response
        newResponse['tempLeadID'] = leads[index]['tempLeadID'] || null
        newResponse['batchID'] = leads[index]['batchID'] || null
        newResponse['vacant'] = leads[index]['vacant'] || null
        newResponse['vacantMailing'] = leads[index]['vacantMailing'] || null
        newResponse['duplicateID'] = leads[index]['duplicateID'] || null
        newResponse['validatedMailingStreet'] = leads[index]['validatedMailingStreet'] || null
        newResponse['validatedMailingCity'] = leads[index]['validatedMailingCity'] || null
        newResponse['validatedMailingZipCode'] = leads[index]['validatedMailingZipCode'] || null
        newResponse['validatedMailingState'] = leads[index]['validatedMailingState'] || null
        newResponse['validatedMailingCounty'] = leads[index]['validatedMailingCounty'] || null
        tracerWithVendorHits += 1
        responsesProcessed.push(newResponse)
    })
    return {vendorTraces: responsesProcessed, tracerWithVendorHits}
}

const getUrl = () => {
    return process.env.IDI_SEARCH_URL
}

const getHeaders = () => {
    return [ 
        `Authorization: ${TOKEN}`,
        'Content-Type: application/json',
        'Accept: application/json'
    ]
}

const getToken = async () => {
    let credentials = getLoginCreds()
    let curlSettings = {   
        url: credentials['url'],
        payload: {
            glba: 'otheruse',
            dppa: 'none'
        },
        headers: {
            Authorization: `Basic ${encode.encode(`${credentials['username']}:${credentials['password']}`, 'base64')}`,
            'Content-Type': 'application/json'
        }
    }
    return await curl('POST', curlSettings)
}

const getLoginCreds = () => {
    return {
        url: process.env.IDI_LOGIN_URL,
        username: process.env.IDI_USERNAME,
        password: process.env.IDI_PASSWORD
    }
}

const leadsToPayload = (leads, tracerType) => {
    return leads.map((lead) => {
        let firstName = (lead.validatedFirstName || lead.firstName || 'none')
        let lastName = (lead.validatedLastName || lead.lastName || 'none')
        let address = (lead.validatedMailingStreet || lead.mailingStreet || 'none')
        let city = (lead.validatedMailingCity || lead.mailingCity || 'none')
        let zip = lead.validatedMailingZipCode || lead.mailingZipCode || 'none'
        let state = (lead.validatedMailingState || lead.mailingZipCode || 'none')
  

        if (tracerType == 'people') 
            return {firstName, lastName, address, city, state, zip, fields: ['name', 'address', 'phone', 'dob', 'email', 'bankruptcy', 'relationshipDetail', 'ip', 'isDead']}
        else if (tracerType == 'entity') 
            return {address, city, state, zip, fields: ['name', 'address', 'phone', 'dob', 'email', 'bankruptcy', 'ip', 'isDead']}
    })
    
}

