var axios = require('axios');

export const doCurls = async (body) => {
    var requests = []
    body.forEach(element => {
        var _headers = {}
        element.headers.forEach((header) => {
            let splitedHeader = header.split(':')
            _headers[splitedHeader[0]] = splitedHeader[0].includes('Authorization') && element.url.includes('idicore.com/search') ? `${splitedHeader[1].substring(1).replace(/['"]+/g, '')}` : splitedHeader[1].substring(1).replace(/['"]+/g, '')
        })
        requests.push(
            axios.post(element.url, JSON.stringify(element.payload), {headers: _headers})
        )
    });
    var responses = await Promise.allSettled(requests)
    var responsesArray = []
    responses.forEach((element) => {
        if (element?.value?.data) {
            responsesArray.push(
                {response: element.value.data}
            )
        }
    })
    return responsesArray
}

export const curl = async (method = 'GET',body) => {
    try {
        let response
        if (method == 'POST') {
            response = await axios.post(body.url, body.payload, {headers: body.headers})
        }
        return response.data
    }catch(e) {
        console.log(e)
        return ''
    }
    
}