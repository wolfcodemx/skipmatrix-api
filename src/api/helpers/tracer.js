var http = require('http');
const fs = require("fs");
const csv = require("fast-csv");

import got from 'got'
import { getRemoteCSV } from '../../utils/csv'
import { random } from '../../utils/tools'
import * as Smarty from '../externalApi/smartyStreets'
import * as IDI from '../externalApi/idi'
import * as Melissa from '../externalApi/melissa'

import _ from 'underscore'
import { getUploadedBatch } from '../factories/userUploadBatches'
import { models } from '../models/initModels'

const columnIDs = {
    "fullName": 0,
    "firstName" : 1,
    "lastName" : 2,
    "propertyStreet" : 3,
    "propertyCity" : 4,
    "propertyState" : 5,
    "propertyZipCode" : 6,
    "mailingStreet" : 7,
    "mailingCity" : 8,
    "mailingState" : 9,
    "mailingZipCode" : 10,
    "parcelNumber" : 11,
    "FIPSCode" : 12,
};

const BATCH_MAX_SIZE = 300;

var TRACER_TYPE = ''
var BATCH_ID = ''
var TEMP_BATCH_ID = ''
var UPLOADED_BATCH = {}
var DUPLICATES_IN_FILE_ARRAY = []
var FINAL_RESULT = {}
var CURRENT_CHUNK = 1

var LEVEL_ID
var TRACED_WITH_MATRIX = 0
var TRACED_WITH_VENDOR = 0
var TRACED_WITH_VENDOR_HITS = 0
var DUPLICATES_IN_DB = 0
var DUPLICATES_IN_FILE = 0
var DUPLICATES_HITS = 0
var LEADS_STORED = []

export const runTrace = async (tracerType, batchID) => {
    initVariable()
    TRACER_TYPE = tracerType
    TEMP_BATCH_ID = batchID
    UPLOADED_BATCH = await getUploadedBatch({batchID})
    try {
        const remoteUrl = await getRemoteCSV(tracerType, batchID);
        let rows = [];

        let process = await new Promise(async function(resolve, reject) {
          await got.stream(remoteUrl)
            .pipe(csv.parse({ headers: false }))
            .on("error", (error) => {
              throw error.message;
            })
            .on("data", (row) => {
              rows.push(row);
            })
            .on("end", async () => {
              resolve(await _run(tracerType, rows, batchID))
            })   
        });
        return process    
      } catch (error) {
        console.log(error);
        res.status(500).send({
          message: "Could not upload the file: " + req.file.originalname,
        });
    }
}

const _run = async (tracerType, rows, batchID) => {
    
    let leads = addExtraInfoToLead(parseCsvToLeads(tracerType, rows), batchID)
    
    if (process.env.LEADS_LIMIT_PEAR_FILE) {
      console.log('Limiting Leads to ' + process.env.LEADS_LIMIT_PEAR_FILE)
      leads = leads.slice(0, process.env.LEADS_LIMIT_PEAR_FILE);
    }
    

    let startedFine = await startBatch(batchID, leads)
    if (startedFine) {
      var i,j, temporary, chunk = BATCH_MAX_SIZE;
      for (let chunkCount = 1, i = 0, j = leads.length; i <= j; i += chunk, chunkCount += 1) {
          temporary = leads.slice(i, i + chunk);
          // do whatever
          CURRENT_CHUNK = chunkCount
          await executeBatch(temporary)
          await endBatch()
      }
      await finishTracer()
    }

    let batchInfo = await models.user_traces_batches.findOne({
      where: {
        batchID: BATCH_ID
      }
    })
    return batchInfo.dataValues
}

const initVariable = () => {
   TRACER_TYPE = ''
   BATCH_ID = ''
   TEMP_BATCH_ID = ''
   UPLOADED_BATCH = {}
   DUPLICATES_IN_FILE_ARRAY = []
   FINAL_RESULT = {}
   CURRENT_CHUNK = 1

   LEVEL_ID
   TRACED_WITH_MATRIX = 0
   TRACED_WITH_VENDOR = 0
   TRACED_WITH_VENDOR_HITS = 0
   DUPLICATES_IN_DB = 0
   DUPLICATES_IN_FILE = 0
   DUPLICATES_HITS = 0
   LEADS_STORED = []
}

const startBatch = async (batchID, leads) => {
  try {
    const batch = await models.user_traces_batches.create({
      tempBatchID: batchID,
      userID: UPLOADED_BATCH.userID,
      name: UPLOADED_BATCH.searchName,
      type: UPLOADED_BATCH.searchType,
      updateDuplicates: UPLOADED_BATCH.updateDuplicates,
      leadType: TRACER_TYPE,
      leads: leads.length,
      customColsInfo: UPLOADED_BATCH.customColumnsInfo
    })  
    BATCH_ID = batch.batchID
    return true
  } catch (e){
    return false
  }
}

const endBatch = async () => {
  let totalHits = LEADS_STORED.filter((element) => element.missedHit == 0).length
  let toUpdate = {
    workingBatchRow: LEADS_STORED.length,
    totalHits,
    uniqueHits: LEADS_STORED.filter((element) => element.missedHit == 0 && !_.isEmpty(element.duplicateID) && element.tracedWithMatrix).length,
    duplicateHits: LEADS_STORED.filter((element) => element.duplicateID != '').length,
    missedHits: LEADS_STORED.filter((element) => element.missedHit != 0).length,
    duplicates: DUPLICATES_IN_FILE + DUPLICATES_IN_DB,
    duplicatesFile: DUPLICATES_IN_FILE,
    duplicatesDB: DUPLICATES_IN_DB,
    hitRate: totalHits / LEADS_STORED.length,
    tracedWithVendor: TRACED_WITH_VENDOR_HITS,
    tracedWithVendorHits: LEADS_STORED.filter((element) => element.missedHit == 0 && _.isEmpty(element.duplicateID) && element.tracedWithMatrix == 0).length, 
    tracedWithMatrixHits:  LEADS_STORED.filter((element) => element.tracedWithMatrix == 1).length
    //relatives: ,
    //associates: ,
    //tracePrice: ,
    //cost: ,
    //savings: ,
    //levelID: ,
  }
  console.log(toUpdate)
  models.user_traces_batches.update(toUpdate, { where: { batchID: BATCH_ID }, })
}

const executeBatch = async (leads) => {
  leads = await Smarty.search(leads)
  leads = findDuplicatesInFile(leads)
  leads = await Smarty.search(leads)
  leads = await traceWithMatrix(leads)
  
  let traces = []
  let leadsTracedWithVendor = leads.filter((element) => !element.tracedWithMatrix)
  let leadsTracedWithMatrix = leads.filter((element) => element.tracedWithMatrix)

  TRACED_WITH_VENDOR += leads.filter((element) => !element.tracedWithMatrix).length
  if (TRACER_TYPE == 'people' || TRACER_TYPE == 'entity') {
    let vendorSearch = await IDI.search(leadsTracedWithVendor, TRACER_TYPE) 
    traces = vendorSearch.vendorTraces
    TRACED_WITH_VENDOR_HITS += vendorSearch.tracerWithVendorHits
  } else if (TRACER_TYPE == 'property') {
    let vendorSearch = await Melissa.search(leadsTracedWithVendor, TRACER_TYPE, UPLOADED_BATCH.searchType) 
    traces = vendorSearch.vendorTraces
    TRACED_WITH_VENDOR_HITS += vendorSearch.tracerWithVendorHits
  }
  
  let finalTraces = prepareTraces(traces, leads)
  let matrixTraces = await fillTracesDuplicated(traces, leads)
  if(!_.isEmpty(matrixTraces.processedTracesOwners))
    finalTraces.processedTracesOwners = [...finalTraces.processedTracesOwners, ...matrixTraces.processedTracesOwners]

  await storeLeads(finalTraces.leads)
  if(TRACER_TYPE == 'people')
  {
    await storeTracesOwners(finalTraces.processedTracesOwners);
    await storeTracesRelations(finalTraces.processedTracesRelations);
  } else if (TRACER_TYPE == 'property') {
    await storeTracesProperties(finalTraces.processedTracesProperties);
  }
}

const parseCsvToLeads = (tracerType, csvData) => {
    return csvData.map((element, index) => {
        let newSubElement = {}
        for(var subElement in columnIDs) {
            newSubElement[subElement] = element[columnIDs[subElement]] 
        }
        return newSubElement
    })
}

const addExtraInfoToLead = (leads, batchID) => {
  return leads.map((lead) => {
    return {
      ...lead,
      tempLeadID: random(15),
      batchID,
      duplicateID: null,
      trueDuplicateID: null,
      dupInFile: false,
      dupInDB: false,
      vacant: 'Unknown',
      vacantMailing: 'Unknown',
      tracedWithMatrix: 0,
      missedHit: 1
    }
  })
}

const findDuplicatesInFile = (leads) => {
  // find duplicate column name by tracer type
  let dupColumnName = ''
  if (TRACER_TYPE == 'people' || TRACER_TYPE =='listCleaner')
    dupColumnName = 'mailingStreet'
  else if (TRACER_TYPE == 'property') {
    if (UPLOADED_BATCH.searchType == 'apn')
      dupColumnName = 'parcelNumber'
    else if(UPLOADED_BATCH.searchType == 'streetCityState' || UPLOADED_BATCH.searchType == 'streetZip')
      dupColumnName = 'propertyStreet'
    else 
      dupColumnName = 'propertyStreet';
  }
  else if(TRACER_TYPE == 'entity')
    dupColumnName = 'mailingStreet';
  else
    return

  return leads.map((lead, index) => {
    let duplicateIndex = leads.findIndex((element) => element[dupColumnName].toLowerCase() == lead[dupColumnName].toLowerCase() )
    if (duplicateIndex != index) {
      lead['duplicateID'] = leads[duplicateIndex]['tempLeadID']
      lead['dupInFile'] = true

      // validate address in duplicates
      lead['validatedMailingStreet'] = leads[duplicateIndex]['validatedMailingStreet']
      lead['validatedMailingCity'] = leads[duplicateIndex]['validatedMailingCity']
      lead['validatedMailingZipCode'] = leads[duplicateIndex]['validatedMailingZipCode']
      lead['validatedMailingState'] = leads[duplicateIndex]['validatedMailingState']
      lead['validatedMailingCounty'] = leads[duplicateIndex]['validatedMailingCounty']

      DUPLICATES_IN_FILE_ARRAY.push({
        originalKey: index,
        duplicateIndex: duplicateIndex
      })
      DUPLICATES_IN_FILE += 1
    }
    return lead
  })
}

export const traceWithMatrix = async (leads) => {
  let table = '', tableUnvalidated = '', arrayAddressColumn = '', sqlAddressColumn = '', sqlAddressColumns = '', arrayAddressColumnUnvalidated = '', sqlAddressColumnUnvalidated = '', sqlAddressColumnsUnvalidated = '', leadType = '';
  let dupColumnNameArray = '', dupColumnNameSql = '';
  if (TRACER_TYPE == 'people')
  {
    table = 'user_traces_owners';
    tableUnvalidated = "user_leads";
    arrayAddressColumn = 'validatedMailingStreet';
    sqlAddressColumn = 'validatedMailingStreet';
    sqlAddressColumns = ['validatedMailingStreet'];
    arrayAddressColumnUnvalidated = 'mailingStreet';
    sqlAddressColumnUnvalidated = 'mailingStreet';
    sqlAddressColumnsUnvalidated = ['mailingStreet'];
    leadType = 'people';
  } else if(TRACER_TYPE == 'property') {
//Correlates with peopleTracer module input values
    if(UPLOADED_BATCH.searchType == 'apn')
    {
        dupColumnNameArray = 'parcelNumber';
        dupColumnNameSql = 'parcelNumber';
    }
    else if(UPLOADED_BATCH.searchType == 'streetCityState' || UPLOADED_BATCH.searchType == 'streetZip')
    {
        dupColumnNameArray = 'propertyStreet';
        dupColumnNameSql = 'propertyStreet';
    }
    else
    {
        dupColumnNameArray = 'propertyStreet';
        dupColumnNameSql = 'propertyStreet';
    }

    table = 'user_traces_property';
    tableUnvalidated = "user_leads";
    arrayAddressColumn = 'validatedPropertyStreet';
    sqlAddressColumn = 'propertyStreet';
    sqlAddressColumns = ['propertyStreet', 'propertyCity', 'propertyState', 'propertyZip', 'verifiedParcelNumber', 'verifiedFIPS'];
    arrayAddressColumnUnvalidated = dupColumnNameArray;
    sqlAddressColumnUnvalidated = dupColumnNameSql;
    sqlAddressColumnsUnvalidated = ['propertyStreet', 'propertyCity', 'propertyState', 'propertyZipCode', 'parcelNumber', 'FIPSCode']
    leadType = 'property';
  } else if(TRACER_TYPE == 'entity') {
    table = 'user_traces_entity';
    tableUnvalidated = "user_leads";
    arrayAddressColumn = 'validatedMailingStreet';
    sqlAddressColumn = 'validatedMailingStreet';
    sqlAddressColumns = ['validatedMailingStreet'];
    arrayAddressColumnUnvalidated = 'mailingStreet';
    sqlAddressColumnUnvalidated = 'mailingStreet';
    sqlAddressColumnsUnvalidated = ['mailingStreet'];
    leadType = 'entity';
  }

  // validated streets
  let validatedStreets = leads.filter((element) => 
    element.duplicateID != '' && !_.isEmpty(element[arrayAddressColumn]) || element[arrayAddressColumn] == ''
  )
  let validatedStreetsArray = validatedStreets.map((element) => {
    return { 
      tempLeadID: element.tempLeadID,
      street: element[arrayAddressColumn].toUpperCase()
    }
  })


  let validatedResult = await models[table].findAll({
    attributes: ['leadID', 'validatedFirstName', 'validatedLastName', ...sqlAddressColumns],
    where: {
      [sqlAddressColumn]: validatedStreetsArray.map((element) => {return element['street']}),
      missedHit: 0
    }
  })
  validatedResult.forEach((validated) => {
    validatedStreetsArray.forEach((validatedStreet) => {
      let leadIndex = leads.findIndex((element) => element.tempLeadID == validatedStreet.tempLeadID)
      let firstName = (leads[leadIndex].firstName || '').toUpperCase()
      let lastName = (leads[leadIndex].lastName || '').toUpperCase()
      let street = (leads[leadIndex].validatedPropertyStreet || leads[leadIndex].propertyStreet).toUpperCase()
      let city = (leads[leadIndex].validatedPropertyCity || leads[leadIndex].propertyCity).toUpperCase()
      let state = (leads[leadIndex].validatedPropertyState || leads[leadIndex].propertyState).toUpperCase()
      let zip = leads[leadIndex].validatedPropertyZipCode || leads[leadIndex].propertyZipCode
      let apn = leads[leadIndex].parcelNumber
      let FIPS = leads[leadIndex].validatedPropertyCountyFIPS || leads[leadIndex].FIPSCode
            
      if (TRACER_TYPE == 'people' && (lastName != validated['validatedLastName'] || firstName != validated['validatedFirstName'])) {
        return
      } else if (TRACER_TYPE == 'property') {
        if (UPLOADED_BATCH.searchType == 'streetCityState') {
          if(street != validated['propertyStreet'] || city != validated['propertyCity'] || state != validated['propertyState'])
            return;
          
        } else if (UPLOADED_BATCH.searchType == 'streetZip') {
          if(street != validated['propertyStreet'] || zip != validated['propertyZip'])
            return
        } else if(UPLOADED_BATCH.searchType == 'apn') {
            if(apn != validated['verifiedParcelNumber'] || FIPS != validated['verifiedFIPS'])
              return;
        }
      }
      
      if (!leads[leadIndex]['trueDuplicateID']){
        DUPLICATES_IN_DB += 1
        TRACED_WITH_MATRIX += 1
      }
      
      leads[leadIndex]['duplicateID'] = validated['leadID']
      leads[leadIndex]['trueDuplicateID'] = validated['leadID']
      leads[leadIndex]['tracedWithMatrix'] = 1;
      //leads[leadIndex]['dupInDB'] = true;
      leads[leadIndex]['missedHit'] = 0;
    })
  })

  // unvalidated streets
  let unvalidatedStreets = leads.filter((element) => 
    element.duplicateID == '' || !element.dupInFile && !_.isEmpty(element[arrayAddressColumnUnvalidated])
  )
  let unvalidatedStreetsArray = unvalidatedStreets.map((element) => {
    return { 
      tempLeadID: element.tempLeadID,
      street: element[arrayAddressColumnUnvalidated].toUpperCase()
    }
  })

  let unvalidatedResult = await models[tableUnvalidated].findAll({
    attributes: ['leadID', 'validatedFirstName', 'validatedLastName',...sqlAddressColumnsUnvalidated],
    where: {
      [sqlAddressColumnUnvalidated]: unvalidatedStreetsArray.map((element) => {return element['street']}),
      leadType: leadType,
      missedHit: 0
    }
  })
  unvalidatedResult.forEach((validated) => {
    unvalidatedStreetsArray.forEach((unvalidatedStreet) => {
      let leadIndex = leads.findIndex((element) => element.tempLeadID == unvalidatedStreet.tempLeadID)
      let firstName = (leads[leadIndex].firstName || '').toUpperCase()
      let lastName = (leads[leadIndex].lastName || '').toUpperCase()
      let street = (leads[leadIndex].propertyStreet).toUpperCase()
      let city = (leads[leadIndex].propertyCity).toUpperCase()
      let state = (leads[leadIndex].propertyState).toUpperCase()
      let zip = leads[leadIndex].propertyZipCode
      let apn = leads[leadIndex].parcelNumber
      let FIPS = leads[leadIndex].FIPSCode
           
      if (TRACER_TYPE == 'people' || TRACER_TYPE == 'entity') {
        if (lastName != validated['validatedLastName'] || firstName != validated['validatedFirstName']) 
          return
      } else if (TRACER_TYPE == 'property') {
        if (UPLOADED_BATCH.searchType == 'streetCityState') {
          if(street != validated['propertyStreet'] || city != validated['propertyCity'] || state != validated['propertyState'])
            return;
          
        } else if (UPLOADED_BATCH.searchType == 'streetZip') {
          if(street != validated['propertyStreet'] || zip != validated['propertyZip'])
            return
        } else if(UPLOADED_BATCH.searchType == 'apn') {
            if(apn != validated['verifiedParcelNumber'] || FIPS != validated['verifiedFIPS'])
              return;
        }
      }
      
      if (!leads[leadIndex]['trueDuplicateID']){
        DUPLICATES_IN_DB += 1
        TRACED_WITH_MATRIX += 1
      }
      

      leads[leadIndex]['duplicateID'] = validated['leadID']
      leads[leadIndex]['trueDuplicateID'] = validated['leadID']
      leads[leadIndex]['tracedWithMatrix'] = 1;
      //leads[leadIndex]['dupInDB'] = true;
      leads[leadIndex]['missedHit'] = 0;
    })
  })
  return leads
}

const fillTracesDuplicated = async (traces, leads) => {
  let processedTracesOwners = [], processedTracesRelations = [], processedTracesProperties = [], processedTracesEntities = []

  leads.filter((element) => element.tracedWithMatrix)
  for (let i = 0; i < leads.length; i++) {
    let table = ''
    if (TRACER_TYPE == 'property')
      table = 'user_traces_property'
    else if (TRACER_TYPE == 'people')
      table = 'user_traces_owners'
    else if (TRACER_TYPE == 'entity')
      table = 'user_traces_entity'


    let trace = await models[table].findOne({
      where: {
        leadID: leads[i].trueDuplicateID
      }
    })
    let traceCloned = {
      ...trace,
      leadID: '',
      tempLeadID: leads[i].tempLeadID
    }
    if (TRACER_TYPE == 'property')
      processedTracesProperties.push(traceCloned)
    if (TRACER_TYPE == 'people') {
      processedTracesOwners.push(traceCloned)
    }
      
  }
  return {leads, processedTracesOwners, processedTracesRelations, processedTracesProperties, processedTracesEntities}
}

const prepareTraces = (traces, leads) => {
  let processedTracesOwners = [], processedTracesRelations = [], processedTracesProperties = [], processedTracesEntities = []

  traces.forEach((trace) => {
    let leadIndex = leads.findIndex((element) => element.tempLeadID == trace.tempLeadID)
    let lead = ''
    let complexResult = {}
    if (TRACER_TYPE == 'people') {
      complexResult = parseTraceResponseOwner(trace,leads[leadIndex])
      processedTracesOwners.push(complexResult.traceDetails)
      leads[leadIndex] = complexResult.lead
      let responseRelations = parseTraceResponseRelations(trace,leads[leadIndex])
      processedTracesRelations.push(responseRelations)
    }
    if (TRACER_TYPE == 'property') {
      complexResult = parseTraceResponseProperties(trace, leads[leadIndex])
      leads[leadIndex] = complexResult.lead
      processedTracesProperties.push(complexResult.traceDetails)
    } 
    if (TRACER_TYPE == 'entity') {
      complexResult = parseTraceResponseEntity(trace, leads[leadIndex])
      leads[leadIndex] = complexResult.lead
      processedTracesEntities.push(complexResult.traceDetails)
    }
    if (!_.isEmpty(lead))
      leads[leadIndex] = lead
  })
  return {leads, processedTracesOwners, processedTracesRelations, processedTracesProperties, processedTracesEntities}
}

const parseTraceResponseOwner = (trace, lead) => {
  let traceDetails = {};

  traceDetails['tempLeadID'] = trace['tempLeadID'];
  traceDetails['batchID'] = trace['batchID'];
  traceDetails['vacant'] = trace['vacant'];
  traceDetails['vacantMailing'] = trace['vacantMailing'];

  if (_.isEmpty(trace.idi['result'])) {
    lead['missedHit'] = 1
    traceDetails['missedHit'] = 1
    return {traceDetails, lead}
  }

  traceDetails['validatedMailingStreet'] = trace.idi.result[0]['address'][0]['complete'] || null;
  traceDetails['validatedMailingCity'] = trace.idi.result[0]['address'][0]['city'] || null;
  traceDetails['validatedMailingState'] = trace.idi.result[0]['address'][0]['state'] || null;
  traceDetails['validatedMailingCounty'] = trace.idi.result[0]['address'][0]['county'] || null;
  
  let zip = trace.idi.result[0]['address'][0]['zip'] || null
  traceDetails['validatedMailingZipCode'] = null;
  traceDetails['validatedMailingZipCode'] = zip;
  let zip4 = trace.idi.result[0]['address'][0]['zip4'] || null;
  if(_.isEmpty(trace.idi.result[0]['address'][0]['zip4']) && trace.idi.result[0]['address'][0]['zip4'] != '')
      traceDetails['validatedMailingZipCode'] = zip+'-'+zip4;
  
  traceDetails['validatedFirstName'] = trace.idi.result[0]['name'][0]['first'] || null;
  traceDetails['validatedMiddleName'] = trace.idi.result[0]['name'][0]['middle'] || null;
  traceDetails['validatedLastName'] = trace.idi.result[0]['name'][0]['last'] || null;
  
  lead['validatedFirstName'] = traceDetails['validatedFirstName'];
  lead['validatedMiddleName'] = traceDetails['validatedMiddleName'];
  lead['validatedLastName'] = traceDetails['validatedLastName'];
  
  traceDetails['age'] = trace.idi.result[0]['dob'][0]['age'] || null;
  traceDetails['firstSeen'] = trace.idi.result[0]['name'][0]['meta']['firstSeen'] || 'NULL';
  traceDetails['lastSeen'] = trace.idi.result[0]['name'][0]['meta']['lastSeen'] || 'NULL';
  //traceDetails['deceased'] = trace.idi.result[0]['isDead'] || ((trace.idi.result[0]['isDead'] == 'true') ? 1 : 0);
  traceDetails['deceased'] = trace.idi.result[0]['isDead'] || 'Unknown';
  traceDetails['bankrupt'] = !_.isEmpty(trace.idi.result[0]['bankruptcy']) ? trace.idi.result[0]['bankruptcy'][0]['filingDate']['data'] ? 'Yes' : 'No' : 'No';
  traceDetails['ipAddress1'] = trace.idi.result[0]['ip'][0]['data'] || null;
  traceDetails['ipAddress2'] = trace.idi.result[0]['ip'][1]['data'] || null;
  traceDetails['ipAddress3'] = trace.idi.result[0]['ip'][2]['data'] || null;
  
  //Get phone details

  
  for(let i = 0; i < 5; i++)
  {
      if(_.isEmpty(trace.idi.result[0]['phone'][i]))
          continue;
      
      traceDetails['phone'+i] = {}
      traceDetails['phone'+i]['number'] =  (trace.idi.result[0]['phone'][i]['number'] || 'NULL').replace(/[^0-9]/, '')
      traceDetails['phone'+i]['type'] = (trace.idi.result[0]['phone'][i]['type'] || null);
      traceDetails['phone'+i]['firstSeen'] = (trace.idi.result[0]['phone'][i]['meta']['firstSeen'] || 'NULL');
      traceDetails['phone'+i]['lastSeen'] = (trace.idi.result[0]['phone'][i]['meta']['lastSeen'] || 'NULL');
  }
  
  //Get email details
  for(let i = 0; i < 5; i++)
  {
      if(_.isEmpty(trace.idi.result[0]['email'][i]))
              continue;
      
      traceDetails['email'+i] = {}
      traceDetails['email'+i]['address'] = (trace.idi.result[0]['email'][i]['data'] || null);
      traceDetails['email'+i]['firstSeen'] = (trace.idi.result[0]['email'][i]['meta']['firstSeen'] || 'NULL');
      traceDetails['email'+i]['lastSeen'] = (trace.idi.result[0]['email'][i]['meta']['lastSeen'] || 'NULL');
  }
  return {traceDetails, lead} 
}

const parseTraceResponseRelations = (trace, lead) => {
  if (_.isEmpty(trace.idi['result'][0]['relationshipDetail']))
    return
  
  let relations = []
  let batchID = trace['batchID'];
  let tempLeadID = trace['tempLeadID'];
  trace.idi['result'][0]['relationshipDetail'].forEach((relation) => {

    let traceDetails = {}

    traceDetails['tempLeadID'] = tempLeadID;
    traceDetails['batchID'] = batchID;

    traceDetails['age'] = relation?.dob?.age || null;
    traceDetails['firstName'] = relation?.name?.first || null;
    traceDetails['lastName'] = relation?.name?.last || null;
    traceDetails['firstSeen'] = relation?.name?.meta?.firstSeen || null;
    traceDetails['lastSeen'] = relation?.name?.meta?.lastSeen || null;
    traceDetails['relationType'] = relation?.type || null;
    
    traceDetails['mailingStreet'] = relation?.address?.complete || null;
    traceDetails['mailingCity'] = relation?.address?.city || null;
    traceDetails['mailingState'] = relation?.address?.state || null;
    traceDetails['mailingZipCode'] = relation?.address?.zip || null;

    //Get phone details
    for(let i = 0; i < 5; i++)
    {
        if(_.isUndefined(relation['phone'][i]))
            continue;
        
        traceDetails['phone'+i] = []
        traceDetails['phone'+i]['number'] = (relation['phone'][i]['number'] || 'NULL').replace(/[^0-9]/, '');
        traceDetails['phone'+i]['type'] = (relation['phone'][i]['type'] || null);
        traceDetails['phone'+i]['firstSeen'] = (relation['phone'][i]['meta']['firstSeen'] || 'NULL');
        traceDetails['phone'+i]['lastSeen'] = (relation['phone'][i]['meta']['lastSeen'] || 'NULL');
    }

    //Get email details
    for(let i = 0; i < 5; i++)
    {
        if(_.isUndefined(relation['email'][i]))
            continue;
        
        traceDetails['email'+i] = []
        traceDetails['email'+i]['address'] = (relation['email'][i]['data'] || null);
        traceDetails['email'+i]['firstSeen'] = (relation['email'][i]['meta']['firstSeen'] || 'NULL');
        traceDetails['email'+i]['lastSeen'] = (relation['email'][i]['meta']['lastSeen'] || 'NULL');
    }
    relations.push(traceDetails)
  })
  return relations
}

const parseTraceResponseProperties =  (trace, lead) => {
  let traceDetails = [];
  if(_.isUndefined(trace['PrimaryOwner']))
  {
      //array_push(this->missedHitsIDs, trace['tempLeadID']);
      lead['missedHit'] = 1;
      traceDetails['missedHit'] = 1;
          
      return {lead, traceDetails};
  }

  traceDetails['tempLeadID'] = trace['tempLeadID'];
  traceDetails['batchID'] = trace['batchID'];
  traceDetails['vacant'] = trace['vacant'] || null;
  traceDetails['vacantMailing'] = trace['vacantMailing'] || null;

  traceDetails['firstName'] = trace['PrimaryOwner']['Name1First'] || null
  traceDetails['middleName'] = trace['PrimaryOwner']['Name1Middle'] || null
  traceDetails['lastName'] = trace['PrimaryOwner']['Name1Last'] || null
  
  lead['validatedFirstName'] = trace['PrimaryOwner']['Name1First'] || null
  lead['validatedMiddleName'] = trace['PrimaryOwner']['Name1Middle'] || null
  lead['validatedLastName'] = trace['PrimaryOwner']['Name1Last'] || null
  
  traceDetails['validatedFirstName'] = trace['PrimaryOwner']['Name1First'] || null
  traceDetails['validatedMiddleName'] = trace['PrimaryOwner']['Name1Middle'] || null
  traceDetails['validatedLastName'] = trace['PrimaryOwner']['Name1Last'] || null
  traceDetails['validatedFullName'] = trace['PrimaryOwner']['Name1Full'] || null

  traceDetails['propertyStreet'] = trace['PropertyAddress']['Address'] || lead['propertyStreet'] || null;
  traceDetails['propertyCity'] = trace['PropertyAddress']['City'] || lead['propertyCity'] || null;
  traceDetails['propertyState'] = trace['PropertyAddress']['State'] || lead['propertyState'] || null;
  traceDetails['propertyZip'] = trace['PropertyAddress']['Zip'] || lead['propertyZipCode'] || null;
                                                                                
  traceDetails['mailingStreet'] = trace['OwnerAddress']['Address'] || lead['mailingStreet'] || null;
  traceDetails['mailingCity'] = trace['OwnerAddress']['City'] || lead['mailingCity'] || null;
  traceDetails['mailingState'] = trace['OwnerAddress']['State'] || lead['mailingState'] || null;
  traceDetails['mailingZip'] = trace['OwnerAddress']['Zip'] || lead['mailingZipCode'] || null;
  
  traceDetails['validatedMailingStreet'] = lead['validatedMailingStreet'] || null;
  traceDetails['validatedMailingCity'] = lead['validatedMailingCity'] || null;
  traceDetails['validatedMailingZipCode'] = lead['validatedMailingZipCode'] || null;
  traceDetails['validatedMailingCounty'] = lead['validatedMailingCounty'] || null;
  traceDetails['validatedMailingState'] = lead['validatedMailingState'] || null;
  
  traceDetails['validatedPropertyStreet'] = lead['validatedPropertyStreet'] || null;
  traceDetails['validatedPropertyCity'] = lead['validatedPropertyCity'] || null;
  traceDetails['validatedPropertyZipCode'] = lead['validatedPropertyZipCode'] || null;
  traceDetails['validatedPropertyCounty'] = lead['validatedPropertyCounty'] || null;
  traceDetails['validatedPropertyState'] = lead['validatedPropertyState'] || null;
  
  //Check if Melissa returned data for this person
  lead['missedHit'] = 0;
  traceDetails['missedHit'] = 0;
  
  
  traceDetails['fullName'] = trace['PrimaryOwner']['Name1Full'] || 'NULL';
  traceDetails['middleName'] = trace['PrimaryOwner']['Name1Middle'] || null;
  
  traceDetails['owner2FullName'] = trace['PrimaryOwner']['Name2Full'] || null;
  traceDetails['owner3FullName'] = trace['SecondaryOwner']['Name3Full'] || null;
  traceDetails['owner4FullName'] = trace['SecondaryOwner']['Name4Full'] || null;
  
  traceDetails['previousOwnerFullName'] = trace['LastDeedOwnerInfo']['Name1Full'] || null;
  
  let trust = trace['PrimaryOwner']['TrustFlag'];
  if(trust == 'Y')
      trust = 'Yes';
  else if(trust == 'N')
      trust = 'No';
  else
      trust = 'Unknown';
  traceDetails['trust'] = trust;
  
  let company = trace['PrimaryOwner']['CompanyFlag'];
  if(company == 'Y')
      company = 'Yes';
  else
      company = 'No';
  traceDetails['company'] = company;
  
  traceDetails['verifiedParcelNumber'] = trace['Parcel']['FormattedAPN'] || null;
  traceDetails['verifiedFIPS'] = trace['Parcel']['FIPSCode'] || null;
  
  traceDetails['propertyCounty'] = trace['Parcel']['County'] || null;
  traceDetails['propertyLegalDescription'] = trace['Legal']['LegalDescription'] || null;
  traceDetails['propertySubdivision'] = trace['Legal']['Subdivision'] || null;
  traceDetails['propertyYearBuilt'] = trace['PropertyUseInfo']['YearBuilt'] || null;
  traceDetails['propertyEffectiveYearBuilt'] = trace['PropertyUseInfo']['YearBuiltEffective'] || null;
  traceDetails['propertyZoning'] = trace['PropertyUseInfo']['ZonedCodeLocal'] || null;
  traceDetails['propertyUse'] = trace['PropertyUseInfo']['PropertyUseGroup'] || null;
  
  traceDetails['propertyLastTransferDate'] = trace['SaleInfo']['LastOwnershipTransferDate'] || null;
  traceDetails['propertyLastSaleDate'] = trace['SaleInfo']['DeedLastSaleDate'] || null;
  traceDetails['propertyLastSalePrice'] = trace['SaleInfo']['DeedLastSalePrice'] || null;
  
  traceDetails['buildingLivingArea'] = trace['PropertySize']['AreaBuilding'] || null;
  traceDetails['buildingGrossArea'] = trace['PropertySize']['AreaGross'] || null;
  traceDetails['buildingFirstFloorSqft'] = trace['PropertySize']['Area1stFloor'] || null;
  traceDetails['buildingSecondFloorSqft'] = trace['PropertySize']['Area2ndFloor'] || null;
  traceDetails['buildingUpperFloorsSqft'] = trace['PropertySize']['AreaUpperFloors'] || null;
  
  traceDetails['lotAcreage'] = trace['PropertySize']['AreaLotAcres'] || null;
  traceDetails['lotSqft'] = trace['PropertySize']['AreaLotSF'] || null;
  traceDetails['atticSqft'] = trace['PropertySize']['AtticArea'] || null;
  traceDetails['basementSqft'] = trace['PropertySize']['BasementArea'] || null;
  traceDetails['basementFinishedSqft'] = trace['PropertySize']['BasementAreaFinished'] || null;
  traceDetails['basementUnfinishedSqft'] = trace['PropertySize']['BasementAreaUnfinished'] || null;
  
  traceDetails['garageParkingSpaces'] = trace['PropertySize']['ParkingGarage'] || 0;
  traceDetails['garageSqft'] = trace['PropertySize']['ParkingGarageArea'] || null;
  
  let carport = trace['PropertySize']['ParkingCarport'];
  if(carport == '1')
      carport = 'Yes';
  else if(carport == '0')
      carport = 'No';
  else
      carport = 'Unknown';
  traceDetails['carport'] = carport;        
  
  traceDetails['poolSqft'] = trace['Pool']['PoolArea'] || null;
  
  traceDetails['bathrooms'] = trace['IntRoomInfo']['BathCount'] || null;
  traceDetails['partialBathrooms'] = trace['IntRoomInfo']['BathPartialCount'] || null;
  traceDetails['bedrooms'] = trace['IntRoomInfo']['BedroomsCount'] || null;
  traceDetails['storiesCount'] = trace['IntRoomInfo']['StoriesCount'] || null;
  traceDetails['fireplaceCount'] = trace['IntAmenities']['FireplaceCount'] || null;
  
  traceDetails['porchSqft'] = trace['ExtAmenities']['PorchArea'] || null;
  traceDetails['patioSqft'] = trace['ExtAmenities']['PatioArea'] || null;
  
  let deck = trace['ExtAmenities']['DeckFlag'];
  if(deck == '1')
      deck = 'Yes';
  else if(deck == '0')
      deck = 'No';
  else
      deck = 'Unknown';
  traceDetails['deck'] = deck;
  
  traceDetails['deckSqft'] = trace['ExtAmenities']['DeckArea'] || null;
  
  traceDetails['propertyEstimatedValue'] = trace['EstimatedValue']['EstimatedValue'] || null;
  traceDetails['propertyEstimatedValueMin'] = trace['EstimatedValue']['EstimatedMinValue'] || null;
  traceDetails['propertyEstimatedValueMax'] = trace['EstimatedValue']['EstimatedMaxValue'] || null;
  traceDetails['confidenceScore'] = trace['EstimatedValue']['ConfidenceScore'] || null;
  traceDetails['valuationDate'] = trace['EstimatedValue']['ValuationDate'] || null;
  
  traceDetails['assessmentYear'] = trace['Tax']['YearAssessed'] || null;
  traceDetails['assessedValueTotal'] = trace['Tax']['AssessedValueTotal'] || null;
  traceDetails['assessedValueImprovements'] = trace['Tax']['AssessedValueImprovements'] || null;
  traceDetails['assessedValueLand'] = trace['Tax']['AssessedValueLand'] || null;
  traceDetails['assessedImprovementsPercent'] = trace['Tax']['AssessedImprovementsPerc'] || null;
  traceDetails['assessedValuePrevious'] = trace['Tax']['PreviousAssessedValue'] || null;
  traceDetails['assessedValueLastUpdated'] = trace['Tax']['AssrLastUpdated'] || null;
  
  traceDetails['marketValueTotal'] = trace['Tax']['MarketValueTotal'] || null;
  traceDetails['marketValueImprovements'] = trace['Tax']['MarketValueImprovements'] || null;
  traceDetails['marketValueLand'] = trace['Tax']['MarketValueLand'] || null;
  traceDetails['marketImprovementsPercent'] = trace['Tax']['MarketImprovementsPerc'] || null;
  
  traceDetails['taxRateArea'] = trace['Tax']['TaxRateArea'] || null;
  traceDetails['taxBilledAmount'] = trace['Tax']['TaxBilledAmount'] || null;
  traceDetails['taxDelinquentYear'] = trace['Tax']['TaxDelinquentYear'] || null;
  traceDetails['lastTaxRollUpdate'] = trace['Tax']['LastTaxRollUpdate'] || null;
  
  traceDetails['mortgageAmount'] = trace['CurrentDeed']['MortgageAmount'] || null;
  traceDetails['mortgageDate'] = trace['CurrentDeed']['MortgageDate'] || null;
  traceDetails['mortgageTerm'] = trace['CurrentDeed']['MortgageTerm'] || null;
  traceDetails['mortgageLenderName'] = trace['CurrentDeed']['LenderName'] || null;
  traceDetails['secondMortgageAmount'] = trace['CurrentDeed']['SecondMortgageAmount'] || null;
  
  traceDetails['resultCodes'] = trace['Results'] || null;
  
  //print_a(traceDetails);
  
  return {lead, traceDetails};
}

const parseTraceResponseEntity = (trace, lead) => {
  // TODO:
  return {lead, traceDetails};
}

const storeLeads = async (leads) => {
  let leadType = TRACER_TYPE
  for (let i = 0; i < leads.length; i++) {
    let lead = leads[i]
    let leadToStore = {
      userID: UPLOADED_BATCH.userID,      
      duplicateID: lead['trueDuplicateID'] || lead['duplicateID'],
      leadType,
      ...lead,
      batchID: BATCH_ID,
      batchChunkID: CURRENT_CHUNK
    }
    try {
      await models.user_leads.create(leadToStore)
      LEADS_STORED.push(leadToStore)
    } catch (e) {
      console.log(e)
    }
    
  }
} 

const storeTracesOwners = async (processedTracesOwners) => {
  for(let i=0; i < processedTracesOwners.length; i++) {
    let trace = processedTracesOwners[i]
    let realLead = await models.user_leads.findOne({
      where: {
        tempLeadID: trace.tempLeadID
      }
    })
    let traceToStore = {
      ...trace,
      deceased: _.isUndefined(trace['deceased']) ? 'Unknown' : _.isEmpty(trace['deceased']) ? 'No' : 'Yes',
      bankrupt: trace['bankrupt'] || 'Unknown',
      vacant: trace['vacant'] || 'Unknown',
      vacantMailing: trace['vacantMailing'] || 'Unknown',
      ownerHasPhone: _.isEmpty(trace['phone0']) ? 'Unknown' : _.isEmpty(trace['phone0']['number']) ? 'No' : 'Yes',
      leadID: realLead.leadID,
      batchID: BATCH_ID,
      userID: UPLOADED_BATCH.userID
    }
    for (let i = 0; i < 5; i++) {
      if (_.isUndefined(trace['phone'+i]))
        continue
      traceToStore[`phone${i+1}`] = trace['phone'+i]?.number || ''
      traceToStore[`phone${i+1}Type`] = trace['phone'+i]?.type || ''
      traceToStore[`phone${i+1}FirstSeen`] = trace['phone'+i]?.firstSeen || ''
      traceToStore[`phone${i+1}LastSeen`] = trace['phone'+i]?.lastSeen || ''
      if (_.isUndefined(trace['email'+i]))
        continue
      traceToStore[`email${i+1}`] = trace['email'+i]?.address || ''
      traceToStore[`email${i+1}FirstSeen`] = trace['email'+i]?.firstSeen || ''
      traceToStore[`email${i+1}LastSeen`] = trace['email'+i]?.lastSeen || ''
    }
    delete traceToStore.phone0
    delete traceToStore.email0
    let userTracesOwner = await models.user_traces_owners.create(traceToStore)
  }
}

const storeTracesRelations = async (processedTracesRelations) => {
  if (_.isUndefined(processedTracesRelations[0]))
    return
  for(let i=0; i < processedTracesRelations[0].length; i++) {
    let trace = processedTracesRelations[0][i]
    let realLead = await models.user_leads.findOne({
      where: {
        tempLeadID: trace.tempLeadID
      }
    })

    let traceToStore = {
      ...trace,
      leadID: realLead.leadID,
      batchID: BATCH_ID,
      userID: UPLOADED_BATCH.userID
    }

    for (let i = 0; i < 5; i++) {
      traceToStore[`phone${i+1}`] = trace['phone'+i]?.number || ''
      traceToStore[`phone${i+1}Type`] = trace['phone'+i]?.type || ''
      traceToStore[`phone${i+1}FirstSeen`] = trace['phone'+i]?.firstSeen || ''
      traceToStore[`phone${i+1}LastSeen`] = trace['phone'+i]?.lastSeen || ''
      
      traceToStore[`email${i+1}`] = trace['email'+i]?.address || ''
      traceToStore[`email${i+1}FirstSeen`] = trace['email'+i]?.firstSeen || ''
      traceToStore[`email${i+1}LastSeen`] = trace['email'+i]?.lastSeen || ''
    }
    delete traceToStore.phone0
    delete traceToStore.email0

    let userTracesOwner = await models.user_traces_relations.create(traceToStore)
  }
}

const storeTracesProperties = async (processedTracesProperties) => {
  for(let i=0; i < processedTracesProperties.length; i++) {
    let trace = processedTracesProperties[i]
    if (_.isUndefined(trace.tempLeadID)) 
      return
    try {
      let realLead = await models.user_leads.findOne({
        where: {
          tempLeadID: trace.tempLeadID
        }
      })
      let userTracesProperty = await models.user_traces_property.create({
        ...trace,
        leadID: realLead.leadID,
        batchID: BATCH_ID,
        userID: UPLOADED_BATCH.userID
      })
    } catch(error) {
      console.log({error, trace})
    }
  }
}

const finishTracer = async () => {
  await models.user_traces_batches.update({finished: 1}, { where: { batchID: BATCH_ID }, })
}