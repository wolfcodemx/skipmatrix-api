import { models } from '../models/initModels'

export const getUploadedBatch = async (condition) => {
    return await models.user_upload_batches.findOne({ where: condition})
}