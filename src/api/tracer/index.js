import { Router } from 'express'
import { middleware as query } from 'querymen'
import { master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
export Tracer, { schema } from './model'

const router = new Router()

/**
 * @api {post} /tracers Create tracer
 * @apiName CreateTracer
 * @apiGroup Tracer
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess {Object} tracer Tracer's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Tracer not found.
 * @apiError 401 master access only.
 */
router.post('/',
  master(),
  create)

/**
 * @api {get} /tracers Retrieve tracers
 * @apiName RetrieveTracers
 * @apiGroup Tracer
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} tracers List of tracers.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/',
  master(),
  query(),
  index)

/**
 * @api {get} /tracers/:id Retrieve tracer
 * @apiName RetrieveTracer
 * @apiGroup Tracer
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess {Object} tracer Tracer's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Tracer not found.
 * @apiError 401 master access only.
 */
router.get('/:id',
  master(),
  show)

/**
 * @api {put} /tracers/:id Update tracer
 * @apiName UpdateTracer
 * @apiGroup Tracer
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess {Object} tracer Tracer's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Tracer not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  master(),
  update)

/**
 * @api {delete} /tracers/:id Delete tracer
 * @apiName DeleteTracer
 * @apiGroup Tracer
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Tracer not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
