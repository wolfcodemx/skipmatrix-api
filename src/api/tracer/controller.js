import { success, notFound } from '../../services/response/'
import { Tracer } from '.'

export const create = ({ body }, res, next) =>
  Tracer.create(body)
    .then((tracer) => tracer.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Tracer.find(query, select, cursor)
    .then((tracers) => tracers.map((tracer) => tracer.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Tracer.findById(params.id)
    .then(notFound(res))
    .then((tracer) => tracer ? tracer.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ body, params }, res, next) =>
  Tracer.findById(params.id)
    .then(notFound(res))
    .then((tracer) => tracer ? Object.assign(tracer, body).save() : null)
    .then((tracer) => tracer ? tracer.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Tracer.findById(params.id)
    .then(notFound(res))
    .then((tracer) => tracer ? tracer.remove() : null)
    .then(success(res, 204))
    .catch(next)
