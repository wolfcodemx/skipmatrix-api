import { Tracer } from '.'

let tracer

beforeEach(async () => {
  tracer = await Tracer.create({})
})

describe('view', () => {
  it('returns simple view', () => {
    const view = tracer.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(tracer.id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = tracer.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(tracer.id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
