import mongoose, { Schema } from 'mongoose'

const tracerSchema = new Schema({}, { timestamps: true })

tracerSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Tracer', tracerSchema)

export const schema = model.schema
export default model
