import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Tracer } from '.'

const app = () => express(apiRoot, routes)

let tracer

beforeEach(async () => {
  tracer = await Tracer.create({})
})

test('POST /tracers 201 (master)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: masterKey })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
})

test('POST /tracers 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /tracers 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /tracers 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /tracers/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${tracer.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(tracer.id)
})

test('GET /tracers/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${tracer.id}`)
  expect(status).toBe(401)
})

test('GET /tracers/:id 404 (master)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})

test('PUT /tracers/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${tracer.id}`)
    .send({ access_token: masterKey })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(tracer.id)
})

test('PUT /tracers/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${tracer.id}`)
  expect(status).toBe(401)
})

test('PUT /tracers/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey })
  expect(status).toBe(404)
})

test('DELETE /tracers/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${tracer.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /tracers/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${tracer.id}`)
  expect(status).toBe(401)
})

test('DELETE /tracers/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
