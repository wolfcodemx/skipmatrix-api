import { Router } from 'express'
import { master } from '../../services/passport'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /curls Create curl
 * @apiName CreateCurl
 * @apiGroup Curl
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess {Object} curl Curl's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Curl not found.
 * @apiError 401 master access only.
 */
router.post('/',
  master(),
  create)

export default router
