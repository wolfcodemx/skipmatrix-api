
var axios = require('axios');

export const create = async ({ body }, res, next) => {
    var requests = []
    console.log(body)
    body.forEach(element => {
        var _headers = {}
        element.headers.forEach((header) => {
            let splitedHeader = header.split(':')
            _headers[splitedHeader[0]] = splitedHeader[0].includes('Authorization') && element.url.includes('idicore.com/search') ? `${splitedHeader[1].substring(1).replace(/['"]+/g, '')}` : splitedHeader[1].substring(1).replace(/['"]+/g, '')
        })
        console.log({url: element.url, data: JSON.stringify(element.payload), config: {headers: _headers}})
        requests.push(
            axios.post(element.url, element.payload, {headers: _headers})
        )
    });
    var responses = await Promise.allSettled(requests)
    var responsesArray = []
    responses.forEach((element) => {
        console.log(element)
        if (element?.value?.data) {
            responsesArray.push(
                {response: JSON.stringify(element.value.data)}
            )
        }
    })
    //console.log(responsesArray)
    res.status(200).json(responsesArray)
}