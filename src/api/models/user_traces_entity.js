const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_traces_entity', {
    contactID: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    leadID: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    userID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    batchID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    missedHit: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "0"
    },
    entryTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    },
    updatedTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    },
    age: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    deceased: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    bankrupt: {
      type: DataTypes.STRING(11),
      allowNull: true,
      defaultValue: "U"
    },
    bankruptDate: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    vacant: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    vacantMailing: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    validatedFirstName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMiddleName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedLastName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMailingStreet: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    validatedMailingCity: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMailingZip: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    validatedMailingCounty: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMailingState: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    validatedMailingFirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMailingLastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ipAddress1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ipAddress2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ipAddress3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ownerHasPhone: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    phone1: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone1Type: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone1FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone1LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone2: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone2Type: {
      type: DataTypes.STRING(20),
      allowNull: true,
      defaultValue: ""
    },
    phone2FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone2LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone3: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone3Type: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone3FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone3LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone4: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone4Type: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone4FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone4LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone5: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone5Type: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone5FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone5LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email1FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email1LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email2FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email2LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email3FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email3LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email4FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email4LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email5: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email5FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email5LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'user_traces_entity',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "contactID" },
        ]
      },
      {
        name: "userID",
        using: "BTREE",
        fields: [
          { name: "userID" },
        ]
      },
      {
        name: "validatedMailingStreet",
        using: "BTREE",
        fields: [
          { name: "validatedMailingStreet" },
        ]
      },
      {
        name: "leadID",
        using: "BTREE",
        fields: [
          { name: "leadID" },
        ]
      },
      {
        name: "batchID",
        using: "BTREE",
        fields: [
          { name: "batchID" },
        ]
      },
    ]
  });
};
