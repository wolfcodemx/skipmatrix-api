const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_traces_batches_copy', {
    batchID: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    linkedBatchID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    peopleRerunBatchID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tempBatchID: {
      type: DataTypes.STRING(20),
      allowNull: true,
      unique: "tempBatchID"
    },
    finished: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    phpPID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    executionTime: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: 0
    },
    recordsPerSecond: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    userID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    workingBatchRow: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    leadType: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    type: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    updateDuplicates: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    levelID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tracePrice: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    cost: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: 0
    },
    savings: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: 0
    },
    refund: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: 0
    },
    leads: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    totalHits: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    uniqueHits: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    duplicateHits: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    missedHits: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    hitRate: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: 0
    },
    tracedWithVendorHits: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    tracedWithMatrixHits: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    tracedWithVendor: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    tracedWithMatrix: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    duplicates: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    duplicatesFile: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    duplicatesDB: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    relatives: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    associates: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    customColsInfo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    entryTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    },
    updateTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    }
  }, {
    sequelize,
    tableName: 'user_traces_batches_copy',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "batchID" },
        ]
      },
      {
        name: "tempBatchID",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "tempBatchID" },
        ]
      },
      {
        name: "userID",
        using: "BTREE",
        fields: [
          { name: "userID" },
        ]
      },
      {
        name: "name",
        using: "BTREE",
        fields: [
          { name: "name" },
        ]
      },
      {
        name: "leadType",
        using: "BTREE",
        fields: [
          { name: "leadType" },
        ]
      },
      {
        name: "entryTime",
        using: "BTREE",
        fields: [
          { name: "entryTime" },
        ]
      },
    ]
  });
};
