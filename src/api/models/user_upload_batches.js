const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_upload_batches', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    userID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    batchID: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: "",
      unique: "batchID"
    },
    leads: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tracerType: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    searchName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    searchType: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    updateDuplicates: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    estimatedCost: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    customColsInfo: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    duplicatesDB: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    duplicatesNew: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    duplicatesTotal: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    duplicatesFile: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    entryTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    },
    updateTime: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'user_upload_batches',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "batchID",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "batchID" },
        ]
      },
    ]
  });
};
