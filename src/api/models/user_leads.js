const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_leads', {
    leadID: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    entryTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    },
    userID: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    batchID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    batchChunkID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tempLeadID: {
      type: DataTypes.BIGINT,
      allowNull: true,
      unique: "tempLeadID"
    },
    duplicateID: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    missedHit: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    tracedWithMatrix: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    leadType: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    fullName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    firstName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    middleName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    lastName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedFirstName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMiddleName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedLastName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    parcelNumber: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FIPSCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    propertyStreet: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyCity: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyState: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyZipCode: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedPropertyStreet: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedPropertyCity: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedPropertyState: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedPropertyZipCode: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedPropertyCounty: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedPropertyFIPSCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    vacantProperty: {
      type: DataTypes.CHAR(10),
      allowNull: true
    },
    mailingStreet: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mailingCity: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mailingState: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mailingZipCode: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMailingStreet: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMailingCity: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMailingState: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMailingZipCode: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMailingCounty: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMailingFIPSCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    vacantMailing: {
      type: DataTypes.CHAR(10),
      allowNull: true
    },
    needsDuplicateIDUpdate: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    updateTime: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'user_leads',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "leadID" },
        ]
      },
      {
        name: "tempLeadID",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "tempLeadID" },
        ]
      },
      {
        name: "userID",
        using: "BTREE",
        fields: [
          { name: "userID" },
        ]
      },
      {
        name: "batchID",
        using: "BTREE",
        fields: [
          { name: "batchID" },
        ]
      },
      {
        name: "duplicateID",
        using: "BTREE",
        fields: [
          { name: "duplicateID" },
        ]
      },
      {
        name: "mailingStreet",
        using: "BTREE",
        fields: [
          { name: "mailingStreet" },
        ]
      },
      {
        name: "propertyStreet",
        using: "BTREE",
        fields: [
          { name: "propertyStreet" },
        ]
      },
      {
        name: "tracedWithMatrix",
        using: "BTREE",
        fields: [
          { name: "tracedWithMatrix" },
        ]
      },
      {
        name: "batchChunkID",
        using: "BTREE",
        fields: [
          { name: "batchChunkID" },
        ]
      },
      {
        name: "needsDuplicateIDUpdate",
        using: "BTREE",
        fields: [
          { name: "needsDuplicateIDUpdate" },
        ]
      },
      {
        name: "firstName",
        using: "BTREE",
        fields: [
          { name: "firstName" },
        ]
      },
      {
        name: "lastName",
        using: "BTREE",
        fields: [
          { name: "lastName" },
        ]
      },
      {
        name: "fullName",
        using: "BTREE",
        fields: [
          { name: "fullName" },
        ]
      },
      {
        name: "parcelNumber",
        using: "BTREE",
        fields: [
          { name: "parcelNumber" },
        ]
      },
      {
        name: "leadType",
        using: "BTREE",
        fields: [
          { name: "leadType" },
        ]
      },
    ]
  });
};
