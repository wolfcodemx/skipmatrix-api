const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('list_phones_336', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    phone: {
      type: DataTypes.BIGINT,
      allowNull: true,
      unique: "phone"
    },
    lock_code: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    complete: {
      type: DataTypes.TINYINT,
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'list_phones_336',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "phone",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "phone" },
        ]
      },
      {
        name: "lock_code",
        using: "BTREE",
        fields: [
          { name: "lock_code" },
        ]
      },
      {
        name: "complete",
        using: "BTREE",
        fields: [
          { name: "complete" },
        ]
      },
      {
        name: "update_time",
        using: "BTREE",
        fields: [
          { name: "update_time" },
        ]
      },
      {
        name: "active",
        using: "BTREE",
        fields: [
          { name: "active" },
        ]
      },
    ]
  });
};
