const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_traces_relations', {
    relationID: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    userID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    leadID: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    batchID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    relationType: {
      type: DataTypes.STRING(5),
      allowNull: true,
      defaultValue: ""
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    },
    updatedTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    },
    firstName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    lastName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    age: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    mailingStreet: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mailingCity: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mailingState: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mailingZipCode: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone1: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone1Type: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone1FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone1LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone2: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone2Type: {
      type: DataTypes.STRING(20),
      allowNull: true,
      defaultValue: ""
    },
    phone2FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone2LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone3: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone3Type: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone3FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone3LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone4: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone4Type: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone4FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone4LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone5: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone5Type: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    phone5FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone5LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email1FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email1LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email2FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email2LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email3FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email3LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email4FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email4LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email5: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email5FirstSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email5LastSeen: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'user_traces_relations',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "relationID" },
        ]
      },
      {
        name: "batchID",
        using: "BTREE",
        fields: [
          { name: "batchID" },
        ]
      },
      {
        name: "userID",
        using: "BTREE",
        fields: [
          { name: "userID" },
        ]
      },
      {
        name: "leadID",
        using: "BTREE",
        fields: [
          { name: "leadID" },
        ]
      },
      {
        name: "mailingStreet",
        using: "BTREE",
        fields: [
          { name: "mailingStreet" },
        ]
      },
    ]
  });
};
