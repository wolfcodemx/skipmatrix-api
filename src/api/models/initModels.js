var initModels = require("./init-models");
import sequelize from "../../services/sequelize"

export const models = initModels(sequelize);