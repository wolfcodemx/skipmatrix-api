const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('system_errors_php', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    errorNumber: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    errorString: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    errorFile: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    errorLine: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    entryTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    }
  }, {
    sequelize,
    tableName: 'system_errors_php',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
