const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_notifications_views', {
    userNotificationViewID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    userNotificationID: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    viewed: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    userID: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'user_notifications_views',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "userNotificationViewID" },
        ]
      },
    ]
  });
};
