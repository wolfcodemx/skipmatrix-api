var DataTypes = require("sequelize").DataTypes;
var _app_faq = require("./app_faq");
var _app_settings_levels = require("./app_settings_levels");
var _beta_access_codes = require("./beta_access_codes");
var _county_list = require("./county_list");
var _list_phones_336 = require("./list_phones_336");
var _system_errors_mysql = require("./system_errors_mysql");
var _system_errors_php = require("./system_errors_php");
var _user_leads = require("./user_leads");
var _user_leads_custom_columns = require("./user_leads_custom_columns");
var _user_notifications = require("./user_notifications");
var _user_notifications_views = require("./user_notifications_views");
var _user_payments = require("./user_payments");
var _user_payments_methods = require("./user_payments_methods");
var _user_traces_batches = require("./user_traces_batches");
var _user_traces_batches_copy = require("./user_traces_batches_copy");
var _user_traces_batches_old = require("./user_traces_batches_old");
var _user_traces_entity = require("./user_traces_entity");
var _user_traces_owners = require("./user_traces_owners");
var _user_traces_property = require("./user_traces_property");
var _user_traces_relations = require("./user_traces_relations");
var _user_upload_batches = require("./user_upload_batches");
var _users = require("./users");

function initModels(sequelize) {
  var app_faq = _app_faq(sequelize, DataTypes);
  var app_settings_levels = _app_settings_levels(sequelize, DataTypes);
  var beta_access_codes = _beta_access_codes(sequelize, DataTypes);
  var county_list = _county_list(sequelize, DataTypes);
  var list_phones_336 = _list_phones_336(sequelize, DataTypes);
  var system_errors_mysql = _system_errors_mysql(sequelize, DataTypes);
  var system_errors_php = _system_errors_php(sequelize, DataTypes);
  var user_leads = _user_leads(sequelize, DataTypes);
  var user_leads_custom_columns = _user_leads_custom_columns(sequelize, DataTypes);
  var user_notifications = _user_notifications(sequelize, DataTypes);
  var user_notifications_views = _user_notifications_views(sequelize, DataTypes);
  var user_payments = _user_payments(sequelize, DataTypes);
  var user_payments_methods = _user_payments_methods(sequelize, DataTypes);
  var user_traces_batches = _user_traces_batches(sequelize, DataTypes);
  var user_traces_batches_copy = _user_traces_batches_copy(sequelize, DataTypes);
  var user_traces_batches_old = _user_traces_batches_old(sequelize, DataTypes);
  var user_traces_entity = _user_traces_entity(sequelize, DataTypes);
  var user_traces_owners = _user_traces_owners(sequelize, DataTypes);
  var user_traces_property = _user_traces_property(sequelize, DataTypes);
  var user_traces_relations = _user_traces_relations(sequelize, DataTypes);
  var user_upload_batches = _user_upload_batches(sequelize, DataTypes);
  var users = _users(sequelize, DataTypes);


  return {
    app_faq,
    app_settings_levels,
    beta_access_codes,
    county_list,
    list_phones_336,
    system_errors_mysql,
    system_errors_php,
    user_leads,
    user_leads_custom_columns,
    user_notifications,
    user_notifications_views,
    user_payments,
    user_payments_methods,
    user_traces_batches,
    user_traces_batches_copy,
    user_traces_batches_old,
    user_traces_entity,
    user_traces_owners,
    user_traces_property,
    user_traces_relations,
    user_upload_batches,
    users,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
