const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('app_settings_levels', {
    levelID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    hits: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    priceBatchPeople: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    priceBatchProperty: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    priceBatchEntity: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    priceBatchListCleaner: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: 0
    },
    priceSingle: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    priceSingleListCleaner: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: 0
    },
    updatedTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    }
  }, {
    sequelize,
    tableName: 'app_settings_levels',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "levelID" },
        ]
      },
    ]
  });
};
