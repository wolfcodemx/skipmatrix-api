const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_traces_property', {
    leadID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    userID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    batchID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    entryTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    },
    missedHit: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "0"
    },
    updatedTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    },
    validatedFullName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedFirstName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedMiddleName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    validatedLastName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    trust: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    company: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    verifiedParcelNumber: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    verifiedFIPS: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyStreet: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyCity: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyState: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyZip: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    vacant: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    vacantMailing: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    mailingStreet: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mailingCity: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mailingState: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mailingZip: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    owner2FullName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    owner3FullName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    owner4FullName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    previousOwnerFullName: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyCounty: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyLegalDescription: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    propertySubdivision: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyYearBuilt: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    propertyEffectiveYearBuilt: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    propertyZoning: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyUse: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyLastTransferDate: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyLastSaleDate: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    propertyLastSalePrice: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    buildingLivingArea: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    buildingGrossArea: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    buildingFirstFloorSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    buildingSecondFloorSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    buildingUpperFloorsSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    lotAcreage: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    lotSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    atticSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    basementSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    basementFinishedSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    basementUnfinishedSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    garageParkingSpaces: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    garageSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    carport: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    poolSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    bathrooms: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    partialBathrooms: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    bedrooms: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    storiesCount: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    fireplaceCount: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    porchSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    patioSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    deck: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    deckSqft: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    propertyEstimatedValue: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    propertyEstimatedValueMin: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    propertyEstimatedValueMax: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    confidenceScore: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    valuationDate: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    assessmentYear: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    assessedValueTotal: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    assessedValueImprovements: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    assessedValueLand: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    assessedImprovementsPercent: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    assessedValuePrevious: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    assessedValueLastUpdated: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    marketValueTotal: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    marketValueImprovements: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    marketValueLand: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    marketImprovementsPercent: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    taxRateArea: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    taxBilledAmount: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    taxDelinquentYear: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    lastTaxRollUpdate: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mortgageAmount: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    mortgageDate: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    mortgageTerm: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    mortgageLenderName: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    secondMortgageAmount: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    resultCodes: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'user_traces_property',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "leadID" },
        ]
      },
      {
        name: "propertyStreet",
        using: "BTREE",
        fields: [
          { name: "propertyStreet" },
        ]
      },
      {
        name: "batchID",
        using: "BTREE",
        fields: [
          { name: "batchID" },
        ]
      },
      {
        name: "userID",
        using: "BTREE",
        fields: [
          { name: "userID" },
        ]
      },
      {
        name: "verifiedFIPS",
        using: "BTREE",
        fields: [
          { name: "verifiedFIPS" },
        ]
      },
      {
        name: "verifiedParcelNumber",
        using: "BTREE",
        fields: [
          { name: "verifiedParcelNumber" },
        ]
      },
    ]
  });
};
