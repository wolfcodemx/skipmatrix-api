const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_leads_custom_columns', {
    customColID: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    leadID: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      unique: "leadID"
    },
    columnData: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    entryTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    }
  }, {
    sequelize,
    tableName: 'user_leads_custom_columns',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "customColID" },
        ]
      },
      {
        name: "leadID",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "leadID" },
        ]
      },
    ]
  });
};
