var http = require('http');
const fs = require("fs");
const csv = require("fast-csv");
var initModels = require("../models/init-models");
import sequelize from "../../services/sequelize"
var models = initModels(sequelize);
import got from 'got'
import { getRemoteCSV } from '../../utils/csv'
import { runTrace } from '../helpers/tracer'

export const create = async (req, res, next) => {
  const { post, tracerType, batchID } = req.body
  let result = await runTrace(tracerType, batchID);
  res.status(201).json({...result})
}
  

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  res.status(200).json({status: 'hola'})

export const show = ({ params }, res, next) =>
  res.status(200).json({})

export const update = ({ body, params }, res, next) =>
  res.status(200).json(body)

export const destroy = ({ params }, res, next) =>
  res.status(204).end()
