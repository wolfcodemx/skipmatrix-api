/* eslint-disable no-unused-vars */
import path from 'path'
import merge from 'lodash/merge'

/* istanbul ignore next */
const requireProcessEnv = (name) => {
  if (!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable')
  }
  return process.env[name]
}

/* istanbul ignore next */
//if (process.env.NODE_ENV !== 'production') {
  const dotenv = require('dotenv-safe')
  dotenv.config({
    path: path.join(__dirname, '../.env'),
    example: path.join(__dirname, '../.env.example')
  })
//}

const config = {
  all: {
    env: process.env.NODE_ENV || 'development',
    root: path.join(__dirname, '..'),
    port: process.env.PORT || 9000,
    ip: process.env.IP || '0.0.0.0',
    apiRoot: process.env.API_ROOT || '',
    masterKey: requireProcessEnv('MASTER_KEY'),
    jwtSecret: requireProcessEnv('JWT_SECRET'),
    mongo: {
      options: {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useCreateIndex: true
      }
    },
    mysql: {
      dialect: "mysql",
      pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      }
    }
  },
  test: { },
  development: {
    mongo: {
      uri: 'mongodb://localhost/skipmatrix-api-dev',
      options: {
        debug: true
      }
    },
    mysql: {
      HOST: "localhost",
      USER: "root",
      PASSWORD: "root",
      DB: "appskip_matrix",
    },
    app: {
      url: 'http://127.0.0.1:8000/'
    }
  },
  staging: {
    mongo: {
      uri: 'mongodb://localhost/skipmatrix-api-dev',
      options: {
        debug: true
      }
    },
    mysql: {
      HOST: "app-test.skipmatrix.com",
      USER: "apptestskip_john",
      PASSWORD: "1596321Pid!",
      DB: "apptestskip_appskip_matrix_staging",
    },
    app: {
      url: 'http://app-test.skipmatrix.com/'
    }
  },
  production: {
    //ip: process.env.IP || undefined,
    //port: process.env.PORT || 3000,
    mongo: {
      uri: process.env.MONGODB_URI || 'mongodb://localhost/skipmatrix-api'
    },
    mysql: {
      HOST: "app.skipmatrix.com",
      USER: "appskip_john",
      PASSWORD: "1596321Pid!",
      DB: "appskip_matrix",
    },
    app: {
      url: 'http://app.skipmatrix.com/'
    }
  }
}

module.exports = merge(config.all, config[config.all.env])
export default module.exports
