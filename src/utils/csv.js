import { app } from '../config'
const fs = require("fs"); // Or `import fs from "fs";` with ESM

const FOLDERS = {
    "base": "TEMP_FILES/",
    "people" : "TEMP_FILES/TracerPeople/",
    "property": "TEMP_FILES/TracerProperty/",
    "entity": "TEMP_FILES/TracerEntity/",
    "listCleaner": "TEMP_FILES/TracerListCleaner/",
}

export const getRemoteCSV = async (tracerType, batchId) => {
    let folder = FOLDERS[tracerType]
    let finalUrl = `${app.url}${folder}${batchId}.csv`
    //finalUrl = fs.existsSync(finalUrl) ? finalUrl : `${app.url}${FOLDERS['base']}${batchId}`
    console.log(finalUrl)
    return finalUrl
}